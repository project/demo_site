<?php
/**
 * @file
 * Describe hooks provided by the Demo Site Tour module.
 *
 * Demo site module is used to compose tour that can be run on more then one page.
 * It is usually used to show users how and what can be done on given site.
 * Module it self is used only for defining settings of tour but not actually to run it.
 *
 * Tour is run by submodule that is integrated in demo_site module as third party library.
 * Submodule is responsible for handling tour itself. Demo Site module supports more then one tour in the single page.
 * Library it self decide which tour should be triggered. Tour can be forced to triggered using query in link tour=tour_name.
 * For example example.com?tour=tour_name_some. It is recommend to use library module for library integration.
 *
 */

/**
 * Define tour library that will be used in module.
 *
 * All tour library use same settings that is provided from module.
 * Those settings will be exposed to javascript using Drupal.settings object.
 *
 * To be able to use third party libraries in module several parameters needs to be defined in array $libraries.
 *
 * @param array $libraries
 *
 *   $libraries['library_name']['external_css'] is link where library css can be dynamically loaded.
 *   $libraries['library_name']['external_css'] is link where library js can be dynamically loaded.
 *   External links are used only if there is no local version library installed and it is checked using library module.
 *   $libraries['library_name']['module_name'] is module name that integrates library.
 *   $libraries['library_name']['module_js'] is javascript file of the module. Usually that is js implementation of tour library not library it self.
 *   $libraries['library_name']['module_css'] is css file of the module not library it self.
 */
function hook_demo_site_get_library_info_alter(array &$libraries) {
  $libraries['hopscotch']['external_css'] = 'cdnjs.cloudflare.com/ajax/libs/hopscotch/0.2.4/css/hopscotch.min.css';
  $libraries['hopscotch']['external_js'] = 'cdnjs.cloudflare.com/ajax/libs/hopscotch/0.2.4/js/hopscotch.min.js';
  $libraries['hopscotch']['module_name'] = 'demo_site_hopscotch';
  $libraries['hopscotch']['module_js'] = 'js/demo_site_hopscotch.js';
  $libraries['hopscotch']['module_css'] = 'css/demo_site_hopscotch.css';
}

/**
 * Alter tour settings before it will be generated on page.
 *
 * @param object $tour
 *   Tour object that will be available on give page for running.
 */
function hook_demo_site_tour_alter(&$tour) {

}