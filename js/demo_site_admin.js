(function($) {
  Drupal.behaviors.demo_site_admin = {
    attach: function(context) {
      $('.demo-site-block-toggle').click(function(env){
        env.preventDefault();
        $('.demo-site-tours').toggleClass('demo-site-block-closed');
      })

    }
  }
})(jQuery);