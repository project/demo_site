(function($) {
  Drupal.behaviors.demo_site_hopscotch = {
    tour_paths:['first','second'],
    attach: function(context) {
      //available tours to run on this page that is forced or autorun
      //if tour is forced that it will be called with query ?tour=name
      var tours = Drupal.settings.DemoSite.tour;
      var current_tour_object = {};
      var current_tour_data = this.get_current_tour_data();
      // Prevent to ajax rescroll.
      var current_tour = this.get_current_tour_running();
      var existing_window = $('.tour-' + current_tour);
      if (existing_window.length > 0) {
        return;
      }
      // Start force tour if there is one.
      current_tour_object = this.start_forced_tour();
      // Start auto run tour if force tour is not started.
      if (current_tour_object == null) {
        current_tour_object = this.start_auto_run_tour();
      }

      // Run current tour. Tour can started as auto run or force.
      if (current_tour_data['step'] > 0) {
        var running_tour = {}
        for (var k in tours){
          running_tour = this.compose_tour(tours[k]);
          if (running_tour.id == current_tour_data['name']) {
            hopscotch.startTour(running_tour);
          }
        }
      }
    },
    get_current_tour_running: function () {
      var tours = Drupal.settings.DemoSite.tour;
      var running_tour = sessionStorage.getItem('hopscotch.tour.state');
      var tour_name = '';
      if (running_tour != null) {
        var tour_name_split = running_tour.split(':');
        tour_name = tour_name_split[0];
      }
      return tour_name;
    },
    start_auto_run_tour: function () {
      var tours = Drupal.settings.DemoSite.tour;
      for (var k in tours){
        var tour_object = this.compose_tour(tours[k]);
        //check if this tour already seen
        var tour_viewed = localStorage.getItem('hopscotch_tours_'+tours[k].name);
        var right_path = this.get_right_path(tours[k]);
        var tour_data = this.get_current_tour_data();
        // Trigger if tour is not viewed and it is on first step
        if (tour_viewed !== 'true' && tours[k].autorun == '1' && tour_data['step'] == 0) {
          // Only if we are on right path for steps
          if (right_path) {
            hopscotch.startTour(tour_object);
            return tour_object;
          }
        }
      }
    },
    start_forced_tour: function () {
      var tours = Drupal.settings.DemoSite.tour;
      for (var k in tours){
        var tour_object = this.compose_tour(tours[k]);
        //check if this tour already seen
        var tour_viewed = localStorage.getItem('hopscotch_tours_'+tours[k].name);
        //restart tour when tour is force to run
        if (tours[k].force) {
          hopscotch.endTour(true);
          hopscotch.startTour(tour_object);
          return tour_object;
        }
      }

    },
    get_right_path: function (tour) {
      var current_tour = this.get_current_tour_data();
      var tour_step = current_tour['step'];
      var current_url_path = window.location.pathname;
      var right_path = true;
      var step_path = '';
      // This is at least second step which means tour already started.
      // Check if this path belongs to current steps
      if (tour_step > 0) {
        step_path = tour.steps[tour_step].path;
      }
      else {
        step_path = tour.start_path;
      }
      //convert front page to link
      if (step_path == '<front>') {
        step_path = '/';
      }
      //trim for trailing  / and beginning /
      if (trimChar(step_path,'/') == trimChar(current_url_path,'/')) {
        right_path = true;
      }
      else {
        right_path = false;
      }

      /**
       * triming string specific character
       * @param string
       * @param charToRemove
       * @returns {*}
       */
      function trimChar(string, charToRemove) {
        while(string.charAt(0)==charToRemove) {
          string = string.substring(1);
        }

        while(string.charAt(string.length-1)==charToRemove) {
          string = string.substring(0,string.length-1);
        }

        return string;
      }

      return right_path;
    },
    get_current_tour_data: function () {
      var data = [];
      var tour_data = hopscotch.getState();
      var tour_name = '';
      var tour_step = 0;
      if (tour_data != null) {
        tour_name = tour_data.split(':')[0];
        tour_step = parseInt(tour_data.split(':')[1]);
      }
      data['name'] = tour_name;
      data['step'] = tour_step;
      return data;
    },
    compose_tour: function(demo_site_tour) {
      //get steps from tour and compose tour fot hopscotch
      var steps = demo_site_tour.steps;
      //compose steps for tour
      var tour_steps = [];

      for (index = 0; index < steps.length; ++index) {

        var title = steps[index].title;
        var content = steps[index].content;
        var target = steps[index].selector;
        var placement = steps[index].placement;
        var next_url = '';
        var current_url_path = window.location.pathname;
        var multi_page = false;
        var index;
        if (index < (steps.length - 1)) {
          next_url = steps[index+1].path;
          if (next_url == '<front>') {
            next_url = '/'
          }

          if (current_url_path != next_url) {
            //this multi page site it will go to next page
            multi_page = true;
            this.tour_paths[index] = next_url;
            tour_steps[index] = {
              title: title,
              content: content,
              target: target,
              placement: placement,
              multipage: multi_page,
              onNext: function(next_url) {

                var current_url_full = window.location.href;
                var current_url = current_url_full.split("?")[0];
                var current_url_query = current_url_full.split("?")[1];

                var currentStep = hopscotch.getCurrStepNum();
                var url_next_step = this.Drupal.behaviors.demo_site_hopscotch.tour_paths[currentStep-1];
                if (currentStep > 0 && url_next_step != null) {
                  window.location = url_next_step;
                }
              }
            };
          }
          else {
            //if we are on same link do not initiate page load
            tour_steps[index] = {
              title: title,
              content: content,
              target: target,
              placement: placement
            };
          }


        }
        else {
          tour_steps[index] = {
            title: title,
            content: content,
            target: target,
            placement: placement
          };
        }


      }

      var tour = {
        id: demo_site_tour.name,
        steps:tour_steps,
        i18n: {
          nextBtn: Drupal.t("Next"),
          prevBtn: Drupal.t("Previous"),
          doneBtn: Drupal.t("Done"),
          skipBtn: Drupal.t("Skip"),
          closeTooltip: Drupal.t("Close")
        },
        onEnd: function() {
          var basePath = Drupal.settings.basePath;
          var current_tour = hopscotch.getCurrTour();
          hopscotch.endTour();
          localStorage.setItem('hopscotch_tours_'+current_tour.id,'true');
          $.ajax(basePath + 'demo_site/ajax/end_current_tour', {async: false});
        },
        onClose: function() {
          var basePath = Drupal.settings.basePath;
          var current_tour = hopscotch.getCurrTour();
          hopscotch.endTour();
          localStorage.setItem('hopscotch_tours_'+current_tour.id,'true');
          $.ajax(basePath + 'demo_site/ajax/end_current_tour', {async: false});
        }
      };
      return tour;
    }
  }
})(jQuery);


