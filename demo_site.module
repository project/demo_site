<?php

/**
 * @file
 * File demo_site.module - module used to create and execute tours.
 */

/**
 * Implements hook_entity_info().
 */
function demo_site_entity_info() {
  // If this is called too early before update runs, it'll fatal.
  if (!module_exists('inline_entity_form')) {
    return array();
  }
  $demo_site_entity_info['demo_site_tour'] = array(
    'label' => t('Demo Site Tour'),
    'entity class' => 'Entity',
    'controller class' => 'DemoSiteEntityController',
    'base table' => 'demo_site_tour',
    'fieldable' => TRUE,
    'exportable' => TRUE,
    'entity keys' => array(
      'id' => 'demo_site_tour_id',
      'label' => 'title',
      'name' => 'name',
    ),
    'static cache' => TRUE,
    'admin ui' => array(
      'path' => 'admin/structure/demo_site_tours',
      'controller class' => 'DemoSiteUIController',
      'file' => 'includes/demo_site.admin.inc',
    ),
    'module' => 'demo_site',
    'access callback' => 'demo_site_admin_access',
    'views controller class' => 'EntityDefaultViewsController',
    'bundles' => array(
      // Entity defaults to entity type for single bundles.
      'demo_site_tour' => array(
        'label' => t('Site Tour'),
        'admin' => array(
          'path' => 'admin/structure/demo_site_tours',
          'access arguments' => array('administer demo site'),
        ),
      ),
    ),
  );
  $demo_site_entity_info['demo_site_step'] = array(
    'label' => t('Site Tour Step'),
    'entity class' => 'Entity',
    'controller class' => 'EntityAPIController',
    'base table' => 'demo_site_step',
    'fieldable' => FALSE,
    'entity keys' => array(
      'id' => 'demo_site_step_id',
      'label' => 'title',
      'name' => 'name',
    ),
    'static cache' => TRUE,
    'module' => 'demo_site',
    'access callback' => 'demo_site_admin_access',
    'views controller class' => 'EntityDefaultViewsController',
    'inline entity form' => array(
      'controller' => 'DemoSiteStepInlineEntityFormController',
    ),
  );
  return $demo_site_entity_info;
}

/**
 * Implements hook_menu().
 */
function demo_site_menu() {
  $items = array();

  $items['admin/structure/demo_site_tours'] = array(
    'title' => 'Demo Site Tours',
    'access arguments' => array('administer demo site tours'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/structure/demo_site_tours/library'] = array(
    'title' => 'Select tour library',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('demo_site_library_settings_form'),
    'access arguments' => array('administer demo site tours'),
    'file' => 'includes/demo_site.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['demo_site/ajax/end_current_tour'] = array(
    'title' => 'AJAX callback to end current tour',
    'page callback' => 'demo_site_end_current_tour',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function demo_site_permission() {
  return array(
    'administer demo site tours' => array(
      'title' => t('Administer demo site tours'),
      'description' => t('Allows a user to add, edit, and delete demo site tours.'),
    ),
  );
}

/**
 * Bootstrap Tour access callback.
 */
function demo_site_admin_access($op, $tour = NULL, $account = NULL) {
  return user_access('administer demo site tours', $account);
}

/**
 * Implements hook_views_api().
 */
function demo_site_views_api() {
  return array(
    'api' => 3,
  );
}

/**
 * Helper function to actually run a tour. Can be called from anywhere.
 */
function demo_site_run_tour($id, $force = FALSE) {

  $tour = demo_site_tour_load($id);

  if (!demo_site_access($tour)) {
    return;
  }
  $first_path = $tour->start_path;
  if (count($tour->steps) > 1) {
    // Set this as the current tour in the session.
    $_SESSION['tour'] = $tour->name;
    $tour->steps[0]->path = $first_path;
  }

  if ($first_path == current_path() || $first_path == request_path() || ($first_path == '<front>' && request_path() == '')) {
    if (empty($_GET['step']) || $_GET['step'] == 0) {
      // We're starting the tour over.
      if (!empty($_SESSION['nexttour'])) {
        unset($_SESSION['nexttour']);
      }
    }
  }
  drupal_alter('demo_site_tour', $tour);

  $step_path = '';
  foreach ($tour->steps as $key => &$step) {
    // If a path isn't specified, then use the path from the previous step.
    if ($step->path) {
      $step_path = $step->path;
    }
    else {
      $step->path = $step_path;
    }
    // Determine we are on the first step of the tour.
    if ($key == 0 && ($step->path == current_path() || $step->path == request_path() || ($step->path == '<front>' && request_path() == ''))) {
      if (!empty($_GET['tour']) && (empty($_GET['step']) || $_GET['step'] == 0)) {
        $tour->isFirstStep = TRUE;
      }
    }
    // Filter user supplied content.
    $step->title = check_plain($step->title);
    $step->content = check_markup($step->content, $step->content_text_format);
  }

  $tour->force = $force;
  $tour->cleanUrls = variable_get('clean_url', 0);

  // Array that contains library name and links for external implementations (css and js link).
  // Get all libraries that can be used.
  $libraries = variable_get('demo_site_library_list', array());
  $selected_lib = variable_get('demo_site_library', NULL);
  // If there is no library selected.
  if (empty($libraries)) {
    drupal_set_message(t('There is no library installed for Demo Site Tour module. Please install at least one multi page tour library.'), 'error');
    return;
  }

  // If selected library is not installed.
  if (empty($libraries[$selected_lib])) {
    drupal_set_message(t('Selected library @name is not installed for Demo Site Tour module. Please install selected library or enable the module.',
      array('@name' => $selected_lib)), 'error');
    return;
  }
  // Get library settings.
  $lib_module_name = $libraries[$selected_lib]['module_name'];
  $lib_js = $libraries[$selected_lib]['module_js'];
  $lib_css = $libraries[$selected_lib]['module_css'];

  // Export settings of tours to js library.
  drupal_add_js(array('DemoSite' => array('tour' => array($tour->name => $tour))), 'setting');

  if (module_exists('libraries') && ($library = libraries_detect($selected_lib)) && !empty($library['installed'])) {
    libraries_load($selected_lib);
  }
  else {
    // Grab the Library JS from CDNJS if the library isn't installed.
    global $base_url;
    $base = parse_url($base_url);
    $external_js = $libraries[$selected_lib]['external_js'];
    $external_css = $libraries[$selected_lib]['external_css'];
    drupal_add_css($base['scheme'] . '://' . $external_css, 'external');
    drupal_add_js($base['scheme'] . '://' . $external_js, 'external');
  }
  drupal_add_js(drupal_get_path('module', $lib_module_name) . '/' . $lib_js);
  drupal_add_css(drupal_get_path('module', $lib_module_name) . '/' . $lib_css);

}

/**
 * Implements hook_page_build().
 */
function demo_site_page_build(&$page) {

  // Try and detect if we are in an AJAX request and bail if so.
  if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    return;
  }

  $tours = demo_site_load_config();
  $force = FALSE;
  $force_tour_name = '';
  if (!empty($_GET['tour'])) {
    $force_tour_name = $_GET['tour'];
  }
  // Export all tours that can be run on current path.
  foreach ($tours as $id => $tour) {
    $has_path = demo_site_has_path($tour);
    // Set variable force to true if user selects one.
    if ($force_tour_name == $tour->name) {
      $force = TRUE;
    }
    if ($has_path) {
      demo_site_run_tour($id, $force);
    }
  }

}

/**
 * Helper function that check if the tour can be run on current path.
 *
 * @param object $tour
 *   Tour object without steps.
 */
function demo_site_has_path($tour) {
  $tour_full = demo_site_tour_load($tour->demo_site_tour_id);
  $path = $tour->start_path;
  if ($path == current_path() || $path == request_path() || ($path == '<front>' && request_path() == '')) {
    return TRUE;
  }
  if (empty($tour_full->steps)) {
    return FALSE;
  }
  foreach ($tour_full->steps as $step) {
    $path = $step->path;
    // Trim path from begging / and end /.
    $path = trim($path, '/');
    if ($path == current_path() || $path == request_path() || ($path == '<front>' && request_path() == '')) {
      return TRUE;
    }
  }
  return FALSE;
}
/**
 * Helper function to end the current tour.
 */
function demo_site_end_current_tour() {
  unset($_SESSION['tour']);
}

/**
 * Helper function to get all bootstrap tour names and ids in an array keyed by id.
 */
function demo_site_load_config($include_disabled = FALSE) {
  if (!$include_disabled) {
    $tours = db_query('SELECT demo_site_tour_id, name, autorun, start_path FROM {demo_site_tour} WHERE enabled = 1')->fetchAllAssoc('demo_site_tour_id');
  }
  else {
    $tours = db_query('SELECT demo_site_tour_id, name, autorun, start_path FROM {demo_site_tour}')->fetchAllAssoc('demo_site_tour_id');
  }

  return $tours;
}

/**
 * Callback function to{bo load a complete bootstrap tour, including all steps, by id.
 */
function demo_site_tour_load($id) {
  $tour = entity_load('demo_site_tour', array($id), array(), FALSE);
  $tour = reset($tour);
  $wrapper = entity_metadata_wrapper('demo_site_tour', $tour);
  $tour->steps = $wrapper->demo_site_step_reference->value();

  return $tour;
}
/**
 * Load all the tours (and caches them).
 */
function demo_site_tour_load_all($reset = FALSE) {
  $all_tours = &drupal_static(__FUNCTION__);
  if (!isset($all_tours)|| $reset) {
    if (!$reset && ($cache = cache_get('demo_site_tours_all'))) {
      $all_tours = $cache->data;
    }
    else {
      $all_tours = entity_load('demo_site_tour');
      foreach ($all_tours as $id => $tour) {
        $wrapper = entity_metadata_wrapper('demo_site', $tour);
        $all_tours[$id]->steps = $wrapper->demo_site_step_reference->value();
      }
      cache_set('demo_site_tours_all', $all_tours);
    }
  }
  return $all_tours;
}

/**
 * Implements hook_bootstrap_tour_insert().
 */
function demo_site_demo_site_tour_insert($tour) {
  demo_site_demo_site_tour_update($tour);
}

/**
 * Implements hook_bootstrap_tour_insert().
 */
function demo_site_demo_site_tour_update($tour) {
  cache_clear_all('bootstrap_tours_all', 'cache');
  drupal_static_reset('bootstrap_tour_load_all');
}

/**
 * Checks access for user for tour.
 *
 * @param object $tour
 *   Demo_site_tour entity.
 * @param object $account
 *   User object.
 *
 * @return bool
 *   True if user has access, false if they don't.
 */
function demo_site_access($tour, $account = NULL) {
  if (!$account) {
    global $user;
    $account = $user;
  }
  if (empty($tour) || empty($tour->enabled)) {
    return FALSE;
  }

  $access = TRUE;

  if ($account->uid != 1 && !empty($tour->roles)) {
    // Compare the tour's roles to the user's roles and if there aren't any overlaps and
    // the user isn't user 1, cancel running the tour.
    $tour_roles = explode(',', $tour->roles);
    $account_roles = array_keys($account->roles);
    $compare = array_intersect($tour_roles, $account_roles);
    if (empty($compare)) {
      $access = FALSE;
    }
  }

  $access_array = module_invoke_all('demo_site_access', $tour, $account, $access);
  return !in_array(FALSE, $access_array, TRUE) && $access;
}

/**
 * Implements hook_block_info().
 */
function demo_site_block_info() {

  $blocks['demo_site_tours_list'] = array(
    'info' => t('Demo Site Tours'),
    'cache' => DRUPAL_NO_CACHE,
    'status' => 0,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function demo_site_block_view($delta = '') {

  if ($delta == 'demo_site_tours_list') {
    $block['subject'] = '';
    $block['content'] = demo_site_block_content($delta);
  }

  return $block;
}

/**
 * Generate content for demo site tours list block.
 *
 * @param string $delta
 *   Block name.
 *
 * @return string
 *   Content of block
 */
function demo_site_block_content($delta) {
  if ($delta == 'demo_site_tours_list') {
    drupal_add_css(drupal_get_path('module', 'demo_site') . '/css/demo_site_admin.css');
    drupal_add_js(drupal_get_path('module', 'demo_site') . '/js/demo_site_admin.js');
    global $user;
    $mark_up = '<a class="demo-site-block-toggle" href="#">&#63;</a>';
    $tours = demo_site_get_users_tours($user);
    if (empty($tours)) {
      $mark_up .= t('No tour available');
    }
    else {
      $mark_up .= '<ul class="demo-site-tours demo-site-block-closed">';
      foreach ($tours as $tour_id => $tour) {
        if ($tour->start_path == '<front>') {
          $url = '/';
        }
        else {
          $url = $tour->start_path;
        }
        $url = $url . '?tour=' . $tour->name;
        $mark_up .= '<li><a href="' . $url . '">' . $tour->title . '</a></li>';
      }
      $mark_up .= '<ul>';
    }
  }
  return $mark_up;
}

/**
 * Get tours that are available to user.
 *
 * @param object $account
 *   Drupal user object.
 *
 * @return mixed
 *   Returns tours loaded in full form.
 */
function demo_site_get_users_tours($account) {
  $roles = $account->roles;
  $tours = array();
  $tours_full = array();
  // Get all.
  $tour_query = db_select('demo_site_tour', 't')->fields('t', array('demo_site_tour_id', 'roles'));
  $tour_result = $tour_query->execute();
  while ($tour = $tour_result->fetchAssoc()) {
    $tour_id = $tour['demo_site_tour_id'];
    $roles_tour = explode(',', $tour['roles']);
    $tour = demo_site_tour_load($tour_id);
    $has_permission = demo_site_access($tour, $account);
    foreach ($roles as $rid => $r_name) {
      if (in_array($rid, $roles_tour)) {
        $has_permission = TRUE;
      }
    }
    if ($has_permission) {
      $tours[$tour_id] = $tour_id;
      // @todo: see if we can use entity_load instead of demo_site_tour_load because it is faster since it not loads steps in full form.
      $tours_full[$tour_id] = $tour;
    }

  }
  return $tours_full;
}
